module.exports = {
  preset: 'ts-jest',
  roots: ["<rootDir>/tests/"],
  testEnvironment: 'node',
  testRegex: '/tests/.*\\.(test|spec)?\\.(ts|tsx)$',
};
