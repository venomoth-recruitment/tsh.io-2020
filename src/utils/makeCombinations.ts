export default (array: Array<any>): Array<any> => {
  const combinations: Array<Array<any>> = [[]];

  for (const value of array) {
    const copy = [...combinations];
    for (const prefix of copy) {
      combinations.push(prefix.concat(value));
    }
  }

  const [, ...result] = combinations;

  return result;
};
