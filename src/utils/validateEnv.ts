import fs from "fs";
import path from "path";
import { cleanEnv, makeValidator, port } from "envalid";

const fileExists = makeValidator((value) => {
  const databasePath = path.join(__dirname, "/../../", value);

  try {
    fs.accessSync(databasePath, fs.constants.F_OK | fs.constants.W_OK);
  } catch (error) {
    console.error(error.message);

    throw new Error(
      `Could not access the database file by given path '${value}'.`
    );
  }

  if (!fs.statSync(databasePath).isFile()) {
    console.error(`Path '${value}' to database does not point to a file.`);

    throw new Error(
      `Given path '${value}' to database does not point to a file.`
    );
  }

  return true;
}, "fileExists");

export default function validateEnv(): void {
  cleanEnv(process.env, {
    SERVER_PORT: port({
      desc: "Application Port - express server listens on this port",
      example: "3000",
    }),
    DATABASE_PATH: fileExists({
      desc: "Path to JSON database file (relative to project main folder)",
      example: "data/db.json",
    }),
  });
}
