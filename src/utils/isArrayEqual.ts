export default (array1: Array<string>, array2: Array<string>): boolean => {
  if (array1.length !== array2.length) {
    return false;
  }

  array1.sort();
  array2.sort();

  return array1.every((value, index) => {
    return value === array2[index];
  });
};
