import GenreRepository from './GenreRepository';
import { StorageDriver } from '../storage';
import { Genre, MovieDatabase } from '../types';

export default class GenreFileRepository implements GenreRepository {
  constructor(private driver: StorageDriver) {}

  public async findAll(): Promise<Genre[]> {
    try {
      const data = await this.driver.read() as MovieDatabase;
      return data.genres || [];
    } catch(error) {
      console.error(error.message);
      throw new Error('Could not read from database.');
    }
  }
}
