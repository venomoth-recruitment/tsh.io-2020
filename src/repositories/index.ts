import GenreRepository from './GenreRepository';
import GenreFileRepository from './GenreFileRepository';
import MovieRepository from "./MovieRepository";
import MovieFileRepository from "./MovieFileRepository";

export {
  GenreRepository,
  GenreFileRepository,
  MovieRepository,
  MovieFileRepository,
};
