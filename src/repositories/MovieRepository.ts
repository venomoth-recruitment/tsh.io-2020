import { Movie, MovieFilter } from "../types";

export default interface MovieRepository {
  findBy(filters: MovieFilter[]): Promise<Movie[]>;
  persist(movie: Movie): Promise<number>
}
