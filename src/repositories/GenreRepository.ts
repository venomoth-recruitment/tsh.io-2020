import { Genre } from "../types";

export default interface GenreRepository {
  findAll(): Promise<Genre[]>;
}
