import MovieRepository from "./MovieRepository";
import { StorageDriver } from "../storage";
import { Movie, MovieDatabase, MovieFilter } from "../types";

export default class MovieFileRepository implements MovieRepository {
  constructor(private driver: StorageDriver) {}

  public async findBy(filters: MovieFilter[]): Promise<Movie[]> {
    try {
      const data = (await this.driver.read()) as MovieDatabase;
      let movies = data.movies || [];

      if (!movies.length) {
        return movies;
      }

      for (const filter of filters) {
        movies = movies.filter(filter);

        if (!movies.length) {
          return movies;
        }
      }

      return movies;
    } catch (error) {
      console.error(error.message);
      throw new Error("Could not read from database.");
    }
  }

  public async persist(movie: Movie): Promise<number> {
    try {
      const data = (await this.driver.read()) as MovieDatabase;

      if (!movie.id) {
        movie.id = this.getNextId(data.movies);
      }

      data.movies.push(movie);

      await this.driver.write(data);

      return movie.id;
    } catch (error) {
      console.error(error.message);
      throw new Error("Could not write to database.");
    }
  }

  private getNextId(movies: Movie[]): number {
    if (!movies.length) {
      return 1;
    }

    const lastId = movies[movies.length - 1].id || 0;

    return lastId + 1;
  }
}
