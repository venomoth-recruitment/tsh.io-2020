import { NextFunction, Request, Response } from "express";
import { HttpError } from 'http-errors';

export default function (
  error: HttpError,
  request: Request,
  response: Response,
  next: NextFunction
): void {
  response.status(error.status).send(error);
}
