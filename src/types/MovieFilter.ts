import Movie from './Movie';

type MovieFilter = (movie: Movie) => boolean;

export default MovieFilter;
