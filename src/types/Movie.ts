import Genre from "./Genre";

type Movie = {
  id: number | null;
  title: string;
  year: string;
  runtime: string;
  genres: Genre[];
  director: string;
  actors: string;
  plot: string;
  posterUrl: string;
}

export default Movie;
