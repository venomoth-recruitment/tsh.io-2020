import Movie from "./Movie";
import Genre from "./Genre";

type MovieDatabase = {
  genres: Genre[],
  movies: Movie[]
};

export default MovieDatabase;
