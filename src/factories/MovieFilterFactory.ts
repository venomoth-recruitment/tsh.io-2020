import { MovieFilter, Movie, Genre } from "../types";
import { isArrayEqual, makeCombinations } from "../utils";

export default class MovieFilterFactory {
  public create(
    genres: Genre[] | null,
    runtime: number | null,
    runtimeTolerance = 10
  ): MovieFilter[] {
    const filters: MovieFilter[] = [];

    if (runtime) {
      filters.push(this.createRuntimeFilter(runtime, runtimeTolerance));
    }

    if (genres) {
      filters.push(this.createGenresFilter(genres));
    }

    return filters;
  }

  private createRuntimeFilter(
    runtime: number,
    runtimeTolerance: number
  ): MovieFilter {
    const minRuntime = runtime - runtimeTolerance;
    const maxRuntime = runtime + runtimeTolerance;

    return (movie: Movie): boolean =>
      parseInt(movie.runtime) >= minRuntime &&
      parseInt(movie.runtime) <= maxRuntime;
  }

  private createGenresFilter(genres: string[]): MovieFilter {
    const genreSets = makeCombinations(genres);

    return (movie: Movie): boolean => {
      for (const genreSet of genreSets) {
        if (isArrayEqual(movie.genres, genreSet)) {
          return true;
        }
      }

      return false;
    };
  }
}
