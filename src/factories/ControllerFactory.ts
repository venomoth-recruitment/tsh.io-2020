import { Controller, GenreController, MovieController } from "../controllers";
import { GenreService, MovieService } from "../services";
import { MovieFileRepository, GenreFileRepository } from "../repositories";
import { FileStorageDriver } from "../storage";
import { Encoding } from "../types";
import { GenreValidator } from "../validators";
import { UniqueSanitizer } from "../sanitizers";
import MovieFilterFactory from "./MovieFilterFactory";

export default class ControllerFactory {
  create(databasePath: string): Controller[] {
    const storageDriver = new FileStorageDriver(new URL(`file://${databasePath}`), Encoding.UTF8);
    const genreRepository = new GenreFileRepository(storageDriver);
    const movieRepository = new MovieFileRepository(storageDriver);
    const movieFilterFactory = new MovieFilterFactory();
    const genreService = new GenreService(genreRepository);
    const movieService = new MovieService(movieRepository, movieFilterFactory);
    const genreValidator = new GenreValidator(genreService);
    const uniqueSanitizer = new UniqueSanitizer();

    return [
      new MovieController(movieService, genreValidator, uniqueSanitizer),
      new GenreController(genreService)
    ];
  }
}
