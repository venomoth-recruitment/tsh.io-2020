import GenreService from "./GenreService";
import MovieService from "./MovieService";

export { GenreService, MovieService };
