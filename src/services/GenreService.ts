import { GenreRepository } from "../repositories";
import { Genre } from "../types";

export default class GenreService {
  constructor(
    private genreRepository: GenreRepository,
  ) {}

  async getGenres(): Promise<Genre[]> {
    try {
      return await this.genreRepository.findAll();
    } catch(error) {
      console.error(error.message);
      throw new Error("Could not get genres.");
    }
  }
}
