import { MovieRepository } from "../repositories";
import { Movie, Genre } from "../types";
import { randomInt } from "../utils";
import { MovieFilterFactory } from "../factories";

export default class MovieService {
  private runtimeTolerance = 10;

  constructor(
    private movieRepository: MovieRepository,
    private movieFilterFactory: MovieFilterFactory
  ) {}

  async createMovie(movie: Movie): Promise<number> {
    try {
      return await this.movieRepository.persist(movie);
    } catch (error) {
      console.error(error.message);
      throw new Error("Could not save movie.");
    }
  }

  async getMovies(
    runtime: number | null,
    genres: Genre[] | null
  ): Promise<Movie[]> {
    const filters = this.movieFilterFactory.create(
      genres,
      runtime,
      this.runtimeTolerance
    );

    try {
      const result = await this.movieRepository.findBy(filters);

      if (!result.length) {
        return result;
      }

      if ((!runtime && !genres) || (runtime && !genres)) {
        return [result[randomInt(0, result.length - 1)]];
      }

      this.sortByGenres(result);

      return result;
    } catch (error) {
      console.error(error.message);
      throw new Error("Could not get movies.");
    }
  }

  private sortByGenres(array: Array<Movie>): void {
    array.sort((a: Movie, b: Movie): number => {
      if (a.genres.length === b.genres.length) {
        const aConcat = a.genres.reduce((concat, item) => concat + item, "");
        const bConcat = b.genres.reduce((concat, item) => concat + item, "");

        return [aConcat, bConcat].sort().indexOf(aConcat) ? 1 : -1;
      }

      return a.genres.length > b.genres.length ? -1 : 1;
    });
  }
}
