export default class UniqueSanitizer {
  public sanitize(value: any): any {
    return [...new Set(value)];
  }
}
