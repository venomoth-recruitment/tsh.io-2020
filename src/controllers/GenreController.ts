import express, { NextFunction, Request, Response } from "express";
import createError from "http-errors";
import Controller from "./Controller";
import { GenreService } from "../services";

export default class GenreController implements Controller {
  private path = "/genres";
  readonly router = express.Router();

  constructor(private genreService: GenreService) {
    this.initializeRoutes();
  }

  private initializeRoutes(): void {
    this.router.get(this.path, this.index);
  }

  public index = async (
    request: Request,
    response: Response,
    next: NextFunction
  ): Promise<void> => {
    try {
      const genres = await this.genreService.getGenres();

      response.send(genres);
    } catch (error) {
      console.error(error.message);
      next(createError(500, "Unexpected error"));
    }
  };
}
