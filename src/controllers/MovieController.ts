import express, { Request, Response, NextFunction } from "express";
import { body, query, validationResult } from "express-validator";
import createError from "http-errors";
import Controller from "./Controller";
import { UniqueSanitizer } from "../sanitizers";
import { MovieService } from "../services";
import { GenreValidator } from "../validators";

export default class MovieController implements Controller {
  private path = "/movies";
  private storeValidationRules = [
    body("title", "Title must be a string with maximum length of 255.")
      .notEmpty()
      .isString()
      .isLength({ min: 1, max: 255 }),
    body("year", "Year must be a number between 1 and 999.")
      .notEmpty()
      .isInt({ min: 1, max: 9999 }),
    body("runtime", "Runtime must be a number between 1 and 999.")
      .notEmpty()
      .isInt({ min: 1, max: 999 }),
    body("genres").custom(this.genreValidator.validate),
    body("director", "Director must be a string with maximum length of 255.")
      .notEmpty()
      .isString()
      .isLength({ min: 1, max: 255 }),
    body("actors", "Actors must be a string.").optional().isString(),
    body("plot", "Plot must be a string.").optional().isString(),
    body("posterUrl", "Poster URL must be an URL.").optional().isURL(),
  ];

  private indexValidationRules = [
    query("duration", "Duration must be a number between 1 and 999.")
      .optional()
      .isInt({ min: 1, max: 999 }),
    query("genres")
      .optional()
      .custom(this.genreValidator.validate)
      .customSanitizer(this.uniqueSanitizer.sanitize),
  ];
  readonly router = express.Router();

  constructor(
    private movieService: MovieService,
    private genreValidator: GenreValidator,
    private uniqueSanitizer: UniqueSanitizer
  ) {
    this.initializeRoutes();
  }

  private initializeRoutes(): void {
    this.router.get(this.path, this.indexValidationRules, this.index);
    this.router.post(this.path, this.storeValidationRules, this.store);
  }

  public index = async (
    request: Request,
    response: Response,
    next: NextFunction
  ): Promise<void> => {
    const errors = validationResult(request);

    if (!errors.isEmpty()) {
      next(createError(400, "Validation errors.", errors.array()));
      return;
    }

    const runtime = request.query.duration
      ? parseInt(request.query.duration as string)
      : null;
    const genres = (request.query.genres as string[]) || null;

    try {
      const movies = await this.movieService.getMovies(runtime, genres);

      response.send(movies);
    } catch (error) {
      console.error(error.message);
      next(createError(500, "Unexpected error."));
    }
  };

  public store = async (
    request: Request,
    response: Response,
    next: NextFunction
  ): Promise<void> => {
    const errors = validationResult(request);

    if (!errors.isEmpty()) {
      next(createError(400, "Validation errors.", errors.array()));
      return;
    }

    try {
      const id: number = await this.movieService.createMovie({
        id: null,
        title: request.body.title,
        year: "" + request.body.year,
        runtime: "" + request.body.runtime,
        genres: request.body.genres.sort(),
        director: request.body.director,
        actors: request.body.actors || "",
        plot: request.body.plot || "",
        posterUrl: request.body.posterUrl || "",
      });

      response.status(201).send({
        id: id,
      });
    } catch (error) {
      console.error(error.message);
      next(createError(500, "Unexpected error."));
    }
  };
}
