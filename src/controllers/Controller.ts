import { Router } from "express";

interface Controller {
  readonly router: Router;
}

export default Controller;
