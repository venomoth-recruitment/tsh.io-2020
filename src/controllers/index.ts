import Controller from "./Controller";
import GenreController from "./GenreController";
import MovieController from "./MovieController";

export { Controller, GenreController, MovieController };
