import { GenreService } from "../services";

export default class GenreValidator {
  constructor(private genreService: GenreService) {}

  public validate = async (genres: any): Promise<any> => {
    if (!Array.isArray(genres)) {
      throw new Error("Genres must be an array.");
    }

    if (!genres.length) {
      throw new Error("You must specify at least one genre.");
    }

    try {
      const availableGenres = await this.genreService.getGenres();

      const validGenres = genres.filter((genre) =>
        availableGenres.includes(genre)
      );

      if (genres.length !== validGenres.length) {
        throw new Error(
          "Given genres are invalid. To get list of valid genres use GET /api/genres endpoint."
        );
      }
    } catch (error) {
      console.log(error.message);
      throw new Error("Internal server error.");
    }

    return true;
  };
}
