import bodyParser from "body-parser";
import createError from "http-errors";
import express, { NextFunction, Request, Response } from "express";
import { Controller } from "./controllers";
import { errorMiddleware } from "./middlewares";

class App {
  public app: express.Application;

  constructor(controllers: Controller[]) {
    this.app = express();

    this.initializeMiddlewares();
    this.initializeControllers(controllers);
    this.initializeRouteNotFoundHandler();
    this.initializeErrorHandler();
  }

  private initializeMiddlewares(): void {
    this.app.use(express.json());
    this.app.use(bodyParser.json());
  }

  private initializeControllers(controllers: Array<Controller>): void {
    controllers.forEach((controller) => {
      this.app.use("/api", controller.router);
    });
  }

  private initializeRouteNotFoundHandler(): void {
    this.app.use((request: Request, ressponse: Response, next: NextFunction) => {
      next(createError(404));
    });
  }

  private initializeErrorHandler(): void {
    this.app.use(errorMiddleware);
  }
}

export default App;
