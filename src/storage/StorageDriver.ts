export default interface StorageDriver {
  read(): Promise<object>
  write(data: object): Promise<void>
}
