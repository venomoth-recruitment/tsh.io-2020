import fs from "fs";
import { Encoding } from '../types';
import StorageDriver from "./StorageDriver";

export default class FileStorageDriver implements StorageDriver {
  constructor(private path: URL, private encoding: Encoding) {}

  public async read(): Promise<object> {
    try {
      const file = await fs.promises.readFile(this.path, this.encoding);
      return JSON.parse(file);
    } catch(error) {
      console.error(error.message);
      throw new Error('Could not read from given file or file is not valid JSON.');
    }
  }

  public async write(data: object): Promise<void> {
    try {
      await fs.promises.writeFile(this.path, JSON.stringify(data, null, 2), this.encoding);
    } catch(error) {
      console.error(error.message);
      throw new Error('Could not write to given file.');
    }
  }
}
