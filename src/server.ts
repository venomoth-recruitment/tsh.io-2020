import dotenv from "dotenv";
import http from "http";
import path from "path";
import App from "./App";
import { validateEnv } from "./utils";
import { ControllerFactory } from "./factories";

validateEnv();
dotenv.config();

const port = process.env.PORT || "3000";
const databasePath = path.join(__dirname, '/../', process.env.DATABASE_PATH || "/data/db.json");

console.info(`Using ${databasePath} as DB file.`);

const controllerFactory = new ControllerFactory();

const app = new App(controllerFactory.create(databasePath));

const server = http.createServer(app.app);
server.listen(port);
