import { MovieService } from "../src/services";
import { MovieFileRepository } from "../src/repositories";
import { MovieFilterFactory } from "../src/factories";

const fakeMovies = [
  {id: 1, title: "t1", year: "y1", runtime: "20", genres: ["1"], director: "d1", actors: "a1", plot: "p1", posterUrl: "u1"},
  {id: 2, title: "t2", year: "y2", runtime: "50", genres: ["1"], director: "d2", actors: "a2", plot: "p2", posterUrl: "u2"},
  {id: 3, title: "t3", year: "y3", runtime: "20", genres: ["2"], director: "d3", actors: "a3", plot: "p3", posterUrl: "u3"},
  {id: 4, title: "t4", year: "y4", runtime: "70", genres: ["2"], director: "d4", actors: "a4", plot: "p4", posterUrl: "u4"},
  {id: 5, title: "t5", year: "y5", runtime: "50", genres: ["3"], director: "d5", actors: "a5", plot: "p5", posterUrl: "u5"},
  {id: 6, title: "t6", year: "y6", runtime: "70", genres: ["3"], director: "d6", actors: "a6", plot: "p6", posterUrl: "u6"},
  {id: 7, title: "t7", year: "y7", runtime: "20", genres: ["4"], director: "d7", actors: "a7", plot: "p7", posterUrl: "u7"},
  {id: 8, title: "t8", year: "y8", runtime: "60", genres: ["5"], director: "d8", actors: "a8", plot: "p8", posterUrl: "u8"},
  {id: 9, title: "t9", year: "y9", runtime: "20", genres: ["1", "2"], director: "d9", actors: "a9", plot: "p9", posterUrl: "u9"},
  {id: 10, title: "t10", year: "y10", runtime: "55", genres: ["1", "2"], director: "d10", actors: "a10", plot: "p10", posterUrl: "u10"},
  {id: 11, title: "t11", year: "y11", runtime: "65", genres: ["1", "3"], director: "d11", actors: "a11", plot: "p11", posterUrl: "u11"},
  {id: 12, title: "t12", year: "y12", runtime: "60", genres: ["1", "4"], director: "d12", actors: "a12", plot: "p12", posterUrl: "u12"},
  {id: 13, title: "t13", year: "y13", runtime: "60", genres: ["2", "3"], director: "d13", actors: "a13", plot: "p13", posterUrl: "u13"},
  {id: 14, title: "t14", year: "y14", runtime: "20", genres: ["1", "2", "3"], director: "d14", actors: "a14", plot: "p14", posterUrl: "u14"},
  {id: 15, title: "t15", year: "y15", runtime: "60", genres: ["1", "2", "3"], director: "d15", actors: "a15", plot: "p15", posterUrl: "u15"},
  {id: 16, title: "t16", year: "y16", runtime: "20", genres: ["2", "3", "4"], director: "d16", actors: "a16", plot: "p16", posterUrl: "u16"},
  {id: 17, title: "t17", year: "y17", runtime: "20", genres: ["1", "2", "4"], director: "d17", actors: "a17", plot: "p17", posterUrl: "u17"},
  {id: 18, title: "t18", year: "y18", runtime: "20", genres: ["1", "2", "3", "4"], director: "d18", actors: "a18", plot: "p18", posterUrl: "u18"},
];

const storageDriver = {
  read: (): Promise<object> => Promise.resolve({genres: [], movies: fakeMovies}),
  write: (data: object): Promise<void> => Promise.resolve()
};
const movieRepository = new MovieFileRepository(storageDriver);
const movieFilterFactory = new MovieFilterFactory();
const movieService = new MovieService(movieRepository, movieFilterFactory);

describe('Movie Service', () => {
  describe('when no filters applied', () => {
    it('should return array with one item', async () => {
      const movies = await movieService.getMovies(null, null);
      expect(Array.isArray(movies)).toBe(true);
      expect(movies.length).toEqual(1);
    });
  });

  describe('when runtime filter applied', () => {
    it('should return array with one item with proper runtime', async () => {
      let movies = await movieService.getMovies(10, null);
      expect(Array.isArray(movies)).toBe(true);
      expect(movies.length).toBe(1);
      expect(movies[0].runtime).toBe("20");

      movies = await movieService.getMovies(30, null);
      expect(Array.isArray(movies)).toBe(true);
      expect(movies.length).toBe(1);
      expect(movies[0].runtime).toBe("20");
    });
  });

  describe('when genres filter applied', () => {
    it('should return array with many items with proper genres', async () => {
      const movies = await movieService.getMovies(null, ["1", "2", "3"]);
      expect(movies).toEqual([
        fakeMovies[14],
        fakeMovies[13],
        fakeMovies[9],
        fakeMovies[8],
        fakeMovies[10],
        fakeMovies[12],
        fakeMovies[1],
        fakeMovies[0],
        fakeMovies[3],
        fakeMovies[2],
        fakeMovies[5],
        fakeMovies[4]
      ]);
    });
  });

  describe('when runtime and genres filter applied', () => {
    it('should return array with many items with proper runtime and genres', async () => {
      const movies = await movieService.getMovies(60, ["1", "2", "3"]);
      expect(movies).toEqual([
        fakeMovies[14],
        fakeMovies[9],
        fakeMovies[10],
        fakeMovies[12],
        fakeMovies[1],
        fakeMovies[3],
        fakeMovies[5],
        fakeMovies[4]
      ]);
    });
  });
});

